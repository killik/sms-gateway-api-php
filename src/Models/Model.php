<?php

namespace Killik\SMSGWayAPI\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Stringable;

class Model implements Arrayable, Jsonable, Stringable
{
    protected array $data;

    public function getData(?string $index = null)
    {
        if (empty($index))
        {
            return $this->data['data'] ?? $this->data;
        }

        return $this->data[$index] ?? $this->data['data'][$index] ?? null;
    }

    public function getId(): int
    {
        return $this->getData('id');
    }

    public static function fromJson(string $json): self
    {
        return static::make(json_decode($json, true));
    }

    public static function make(array $data): self
    {
        return new static($data);
    }

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function toArray(): array
    {
        return $this->data;
    }

    public function toJson($options = 0): string
    {
        return json_encode($this->toArray(), $options);
    }

    public function __toString(): string
    {
        return $this->toJson();
    }

    public function getCreatedAt(): ?Carbon
    {
        return empty($this->getData('created_at')) ? null : new Carbon($this->getData('created_at'));
    }

    public function getUpdatedAt(): ?Carbon
    {
        return empty($this->getData('updated_at')) ? null : new Carbon($this->getData('updated_at'));
    }
}
