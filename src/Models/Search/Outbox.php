<?php

namespace Killik\SMSGWayAPI\Models\Search;

use Illuminate\Support\Collection;
use Killik\SMSGWayAPI\Models\SMS\Outbox as SMSOutbox;

class Outbox extends Model
{
    public function getResults(): Collection
    {
        return $this->results ?? $this->results = parent::getResults()->map(fn (array $data) => SMSOutbox::make($data));
    }
}
