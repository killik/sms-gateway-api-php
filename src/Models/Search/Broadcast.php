<?php

namespace Killik\SMSGWayAPI\Models\Search;

use Illuminate\Support\Collection;
use Killik\SMSGWayAPI\Models\SMS\Bulk\Broadcast as BulkBroadcast;

class Broadcast extends Model
{
    protected Collection $results;

    public function getResults(): Collection
    {
        return $this->results ?? $this->results = parent::getResults()->map(fn (array $data) => BulkBroadcast::make($data));
    }
}
