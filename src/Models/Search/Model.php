<?php

namespace Killik\SMSGWayAPI\Models\Search;

use Illuminate\Support\Collection;
use Killik\SMSGWayAPI\Models\Generic\Pager;
use Killik\SMSGWayAPI\Models\Model as BaseModel;

class Model extends BaseModel
{
    protected Collection $results;

    public function getResults(): Collection
    {
        return $this->results ?? $this->results = collect($this->getData());
    }

    public function getPager(): ?Pager
    {
        $pager = $this->getData('pager');
        
        return empty($pager) ? null : Pager::make($pager);
    }
}
