<?php

namespace Killik\SMSGWayAPI\Models\Search;

use Illuminate\Support\Collection;
use Killik\SMSGWayAPI\Models\SMS\Bulk\Bulk as BulkBulk;

class Bulk extends Model
{
    protected Collection $results;

    public function getResults(): Collection
    {
        return $this->results ?? $this->results = parent::getResults()->map(fn (array $data) => BulkBulk::make($data));
    }
}
