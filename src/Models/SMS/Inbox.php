<?php

namespace Killik\SMSGWayAPI\Models\SMS;

use Carbon\Carbon;
use Killik\SMSGWayAPI\Models\Model;

class Inbox extends Model
{
    public function getMessage(): string
    {
        return $this->getData('message');
    }

    public function getSender(): string
    {
        return $this->getData('sender');
    }

    public function getReceivedAt(): ?Carbon
    {
        return empty($this->getData('received_at')) ? null : new Carbon($this->getData('received_at'));
    }
}
