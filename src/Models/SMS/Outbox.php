<?php

namespace Killik\SMSGWayAPI\Models\SMS;

use Carbon\Carbon;
use Killik\SMSGWayAPI\Models\Model;

class Outbox extends Model
{
    public function getMessage(): string
    {
        return $this->getData('message');
    }
    
    public function getStatus(): string
    {
        return $this->getData('status');
    }
    
    public function getDestination(): string
    {
        return $this->getData('destination');
    }

    public function getSentAt(): ?Carbon
    {
        return empty($this->getData('sent_at')) ? null : new Carbon($this->getData('sent_at'));
    }
}
