<?php

namespace Killik\SMSGWayAPI\Models\SMS\Bulk;

use Illuminate\Support\Collection;

class Broadcast extends Model
{
    public function getMessage(): string
    {
        return $this->getData('message');
    }

    public function getDestinations(): Collection
    {
        return collect($this->getData('sender'));
    }
}
