<?php

namespace Killik\SMSGWayAPI\Models\SMS\Bulk;

use Illuminate\Support\Collection;
use Killik\SMSGWayAPI\Models\Model as BaseModel;
use Killik\SMSGWayAPI\Models\SMS\Outbox;

class Model extends BaseModel
{
    public function getOutbox(): Collection
    {
        return collect($this->getData('outbox'))->map(fn (array $outbox) => Outbox::make($outbox));
    }
}
