<?php

namespace Killik\SMSGWayAPI\Models\SMS\Bulk;

use Illuminate\Support\Collection;

class Bulk extends Model
{
    public function getMessages(): Collection
    {
        return collect($this->getData('messages'))->map(fn (array $message) => (object) $message);
    }
}
