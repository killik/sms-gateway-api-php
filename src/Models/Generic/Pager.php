<?php namespace Killik\SMSGWayAPI\Models\Generic;

use Killik\SMSGWayAPI\Models\Model;

class Pager extends Model
{
    public function getCurrent(): int
    {
        return $this->getData('current');
    }

    public function getLast(): int
    {
        return $this->getData('last');
    }

    public function getLimit(): int
    {
        return $this->getData('limit');
    }

    public function getOffset(): int
    {
        return $this->getData('offset');
    }

    public function getTotal(): int
    {
        return $this->getData('total');
    }
}