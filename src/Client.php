<?php

namespace Killik\SMSGWayAPI;

use GuzzleHttp\Client as GuzzleHttpClient;
use Killik\SMSGWayAPI\Endpoints\SMS\Broadcast;
use Killik\SMSGWayAPI\Endpoints\SMS\Bulk;
use Killik\SMSGWayAPI\Endpoints\SMS\Inbox;
use Killik\SMSGWayAPI\Endpoints\SMS\Outbox;
use Killik\SMSGWayAPI\Endpoints\Whatsapp\Outbox as OutboxWA;

class Client extends GuzzleHttpClient
{
    protected Outbox $outbox;
    protected OutboxWA $outbox_whatsapp;
    protected Inbox $inbox;
    protected Bulk $bulk;
    protected Broadcast $broadcast;

    public function __construct(string $base_uri, string $token)
    {
        parent::__construct([

            'base_uri' => $base_uri,

            'headers' => [

                'Accept' => 'application/json',
                'Authorization' => sprintf('Bearer %s', $token)
            ]
        ]);
    }

    public function outbox(): Outbox
    {
        return $this->outbox ?? $this->outbox = new Outbox($this);
    }

    public function outbox_whatsapp(): OutboxWA
    {
        return $this->outbox_whatsapp ?? $this->outbox_whatsapp = new OutboxWA($this);
    }

    public function inbox(): Inbox
    {
        return $this->inbox ?? $this->inbox = new Inbox($this);
    }

    public function bulk(): Bulk
    {
        return $this->bulk ?? $this->bulk = new Bulk($this);
    }

    public function broadcast(): Broadcast
    {
        return $this->broadcast ?? $this->broadcast = new Broadcast($this);
    }
}
