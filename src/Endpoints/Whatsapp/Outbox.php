<?php

namespace Killik\SMSGWayAPI\Endpoints\Whatsapp;

use Killik\SMSGWayAPI\Endpoints\Endpoint;
use Killik\SMSGWayAPI\Models\Search\Outbox as SearchOutbox;
use Killik\SMSGWayAPI\Models\SMS\Outbox as OutboxModel;

class Outbox extends Endpoint
{
    public function index(int $limit, int $page, ?string $status = null): SearchOutbox
    {
        return $this->find(null, $limit, $page, $status);
    }

    public function find(?string $query, int $limit, int $page, ?string $status = null): SearchOutbox
    {
        $response = $this->client->get('/api/v1/whatsapp/outbox', [

            'query' => compact('limit', 'page', 'status', 'query')
        ]);

        return SearchOutbox::fromJson($response->getBody());
    }

    public function read(int $id): OutboxModel
    {
        $response = $this->client->get(sprintf('/api/v1/whatsapp/outbox/%s', $id));

        return OutboxModel::fromJson($response->getBody());
    }

    public function store(string $destination, string $message): OutboxModel
    {
        $response = $this->client->post('/api/v1/whatsapp/outbox', [

            'json' => compact('destination', 'message')
        ]);

        return OutboxModel::fromJson($response->getBody());
    }
}
