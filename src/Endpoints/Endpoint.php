<?php

namespace Killik\SMSGWayAPI\Endpoints;

use Killik\SMSGWayAPI\Client;

abstract class Endpoint
{
    protected Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}
