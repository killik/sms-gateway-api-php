<?php

namespace Killik\SMSGWayAPI\Endpoints\SMS;

use Killik\SMSGWayAPI\Endpoints\Endpoint;
use Killik\SMSGWayAPI\Models\Search\Bulk as SearchBulk;
use Killik\SMSGWayAPI\Models\SMS\Bulk\Bulk as BulkBulk;

class Bulk extends Endpoint
{
    public function index(int $limit, int $page, ?string $status = null): SearchBulk
    {
        return $this->find(null, $limit, $page, $status);
    }

    public function find(?string $query, int $limit, int $page, ?string $status = null): SearchBulk
    {
        $response = $this->client->get('/api/v1/sms/bulk', [

            'query' => compact('limit', 'page', 'status', 'query')
        ]);

        return SearchBulk::fromJson($response->getBody());
    }

    public function read(int $id): BulkBulk
    {
        $response = $this->client->get(sprintf('/api/v1/sms/bulk/%s', $id));

        return BulkBulk::fromJson($response->getBody());
    }

    public function store(array $messages): BulkBulk
    {
        $response = $this->client->post('/api/v1/sms/bulk', [

            'json' => compact('messages')
        ]);

        return BulkBulk::fromJson($response->getBody());
    }
}
