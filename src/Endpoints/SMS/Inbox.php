<?php

namespace Killik\SMSGWayAPI\Endpoints\SMS;

use Killik\SMSGWayAPI\Endpoints\Endpoint;
use Killik\SMSGWayAPI\Models\Search\Inbox as SearchInbox;
use Killik\SMSGWayAPI\Models\SMS\Inbox as SMSInbox;

class Inbox extends Endpoint
{
    public function index(int $limit, int $page): SearchInbox
    {
        return $this->find(null, $limit, $page);
    }

    public function find(?string $query, int $limit, int $page): SearchInbox
    {
        $response = $this->client->get('/api/v1/sms/inbox', [

            'query' => compact('limit', 'page', 'query')
        ]);

        return SearchInbox::fromJson($response->getBody());
    }

    public function read(int $id): SMSInbox
    {
        $response = $this->client->get(sprintf('/api/v1/sms/outbox/%s', $id));

        return SMSInbox::fromJson($response->getBody());
    }
}
