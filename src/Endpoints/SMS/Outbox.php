<?php

namespace Killik\SMSGWayAPI\Endpoints\SMS;

use Killik\SMSGWayAPI\Endpoints\Endpoint;
use Killik\SMSGWayAPI\Models\Search\Outbox as SearchOutbox;
use Killik\SMSGWayAPI\Models\SMS\Outbox as SMSOutbox;

class Outbox extends Endpoint
{
    public function index(int $limit, int $page, ?string $status = null): SearchOutbox
    {
        return $this->find(null, $limit, $page, $status);
    }

    public function find(?string $query, int $limit, int $page, ?string $status = null): SearchOutbox
    {
        $response = $this->client->get('/api/v1/sms/outbox', [

            'query' => compact('limit', 'page', 'status', 'query')
        ]);

        return SearchOutbox::fromJson($response->getBody());
    }

    public function read(int $id): SMSOutbox
    {
        $response = $this->client->get(sprintf('/api/v1/sms/outbox/%s', $id));

        return SMSOutbox::fromJson($response->getBody());
    }

    public function store(string $destination, string $message): SMSOutbox
    {
        $response = $this->client->post('/api/v1/sms/outbox', [

            'json' => compact('destination', 'message')
        ]);

        return SMSOutbox::fromJson($response->getBody());
    }
}
