<?php

namespace Killik\SMSGWayAPI\Endpoints\SMS;

use Killik\SMSGWayAPI\Endpoints\Endpoint;
use Killik\SMSGWayAPI\Models\Search\Broadcast as SearchBroadcast;
use Killik\SMSGWayAPI\Models\SMS\Bulk\Broadcast as BulkBroadcast;

class Broadcast extends Endpoint
{
    public function index(int $limit, int $page, ?string $status = null): SearchBroadcast
    {
        return $this->find(null, $limit, $page, $status);
    }

    public function find(?string $query, int $limit, int $page, ?string $status = null): SearchBroadcast
    {
        $response = $this->client->get('/api/v1/sms/broadcast', [

            'query' => compact('limit', 'page', 'status', 'query')
        ]);

        return SearchBroadcast::fromJson($response->getBody());
    }

    public function read(int $id): BulkBroadcast
    {
        $response = $this->client->get(sprintf('/api/v1/sms/broadcast/%s', $id));

        return BulkBroadcast::fromJson($response->getBody());
    }

    public function store(array $destinations, string $message): BulkBroadcast
    {
        $response = $this->client->post('/api/v1/sms/broadcast', [

            'json' => compact('destinations', 'message')
        ]);

        return BulkBroadcast::fromJson($response->getBody());
    }
}
